import random
import os
import colorama
import platform

from colorama import Fore, Back, Style
from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController
from torpydo.telemetryclient import TelemetryClient
import sys


ORANGE = '"\033[38;5;214m'
ORANGE_RESET = '033[0m'
print("Starting")

myFleet = []
enemyFleet = []
debug = None

def main():
    if len(sys.argv) > 1 and sys.argv[1] == 'debug':
        global debug
        debug = True

    if not debug:
        TelemetryClient.init()
        TelemetryClient.trackEvent('ApplicationStarted', {'custom_dimensions': {'Technology': 'Python'}})
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def print_color(string, color):
    print(color + string + Fore.RESET)


def is_game_over(fleet):
    game_over = True
    for ship in fleet:
        game_over = game_over and not ship.alive
    return game_over


def start_game():
    global myFleet, enemyFleet
    # clear the screen
    if(platform.system().lower()=="windows"):
        cmd='cls'
    else:
        cmd='clear'   
    os.system(cmd)
    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    while True:
        print()
        print("Player, it's your turn")
        print("Enemy floating ships:")
        for ship in enemyFleet:
            if ship.alive:
                print('    ', ship.game_print())
        print("Sunked ships:")
        for ship in enemyFleet:
            if not ship.alive:
                print('    ', ship.game_print())
        print("Next action: SHOOT")
        position = parse_position(input("Enter coordinates for your shot :"))
        is_hit = GameController.check_is_hit(enemyFleet, position)
        if is_hit:

            print_color(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''', ORANGE)

        if is_hit:
            print_color("Yeah ! Nice hit !", ORANGE)
        else:
            print_color("Miss", Fore.BLUE)
        if not debug:
            TelemetryClient.trackEvent('Player_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})

        if is_game_over(enemyFleet):
            print("Congratulations! You win!")
            break

        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        print()
        print(f"Computer shoot in {str(position)} and {'hit your ship!' if is_hit else 'miss'}")
        if not debug:
            TelemetryClient.trackEvent('Computer_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})
        if is_hit:
            print_color(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''', ORANGE)
        if is_game_over(myFleet):
            print("Sorry! You lose!")
            break

        print('===================== End round =======================')

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_enemyFleet()
    if debug:
        for ship in enemyFleet:
            print(ship)

    initialize_myFleet()


def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()
    if debug:
        myFleet[0].add_position('B4')
        myFleet[0].add_position('B5')
        myFleet[0].add_position('B6')
        myFleet[0].add_position('B7')
        myFleet[0].add_position('B8')

        myFleet[1].add_position('E6')
        myFleet[1].add_position('E7')
        myFleet[1].add_position('E8')
        myFleet[1].add_position('E9')

        myFleet[2].add_position('A3')
        myFleet[2].add_position('B3')
        myFleet[2].add_position('C3')

        myFleet[3].add_position('F8')
        myFleet[3].add_position('G8')
        myFleet[3].add_position('H8')

        myFleet[4].add_position('C5')
        myFleet[4].add_position('C6')
        return
    
    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print()
        print(f"Please enter the positions for the {ship.name} (size: {ship.size})")

        for i in range(ship.size):
            position_input = input(f"Enter position {i+1} of {ship.size} (i.e A3):")
            ship.add_position(position_input)
            if not debug:
                TelemetryClient.trackEvent('Player_PlaceShipPosition', {'custom_dimensions': {'Position': position_input, 'Ship': ship.name, 'PositionInShip': i}})

def initialize_enemyFleet():
    global enemyFleet

    # shipSizes = [5, 4, 3, 3, 2]
    # letters = [Letter.A, Letter.B, Letter.C, Letter.D, Letter.E, Letter.F, Letter.G, Letter.H]
    #
    # for ship in shipSizes:
    #     vert = random.randint(0, 1)
    #     if vert:
    #         l = letters[random.randint(8 - ship)]
    #         num = random.randint(8)
    #     else:
    #         l = letters[random.randint(8)]
    #         num = random.randint(8 - ship)


    start_positions = [
        [
            [
                (Letter.B, 4),
                (Letter.B, 5),
                (Letter.B, 6),
                (Letter.B, 7),
                (Letter.B, 8),

            ],
            [
                (Letter.E, 6),
                (Letter.E, 7),
                (Letter.E, 8),
                (Letter.E, 9),
            ],
            [
                (Letter.A, 3),
                (Letter.B, 3),
                (Letter.C, 3),
            ],
            [
                (Letter.F, 8),
                (Letter.G, 8),
                (Letter.H, 8),
            ],
            [
                (Letter.C, 5),
                (Letter.C, 6),
             ],
        ],
        [
            [
                (Letter.C, 2),
                (Letter.D, 2),
                (Letter.E, 2),
                (Letter.F, 2),
                (Letter.G, 2),

            ],
            [
                (Letter.A, 2),
                (Letter.A, 3),
                (Letter.A, 4),
                (Letter.A, 5),
            ],
            [
                (Letter.A, 6),
                (Letter.B, 6),
                (Letter.C, 6),
            ],
            [
                (Letter.A, 8),
                (Letter.B, 8),
                (Letter.C, 8),
            ],
            [
                (Letter.E, 4),
                (Letter.E, 5),
            ],
        ],

        [
            [
                (Letter.D, 3),
                (Letter.E, 3),
                (Letter.F, 3),
                (Letter.G, 3),
                (Letter.H, 3),

            ],
            [
                (Letter.D, 1),
                (Letter.E, 1),
                (Letter.F, 1),
                (Letter.G, 1),
            ],
            [
                (Letter.A, 6),
                (Letter.A, 7),
                (Letter.A, 8),
            ],
            [
                (Letter.D, 6),
                (Letter.E, 6),
                (Letter.F, 6),
            ],
            [
                (Letter.E, 8),
                (Letter.F, 8),
            ],
        ],

        [
            [
                (Letter.E, 2),
                (Letter.E, 3),
                (Letter.E, 4),
                (Letter.E, 5),
                (Letter.E, 6),

            ],
            [
                (Letter.A, 5),
                (Letter.A, 6),
                (Letter.A, 7),
                (Letter.A, 8),
            ],
            [
                (Letter.C, 5),
                (Letter.C, 6),
                (Letter.C, 7),
            ],
            [
                (Letter.B, 1),
                (Letter.C, 1),
                (Letter.D, 1),
            ],
            [
                (Letter.F, 8),
                (Letter.G, 8),
            ],
        ],

        [
            [
                (Letter.A, 8),
                (Letter.B, 8),
                (Letter.C, 8),
                (Letter.D, 8),
                (Letter.E, 8),

            ],
            [
                (Letter.D, 3),
                (Letter.D, 4),
                (Letter.D, 5),
                (Letter.D, 6),
            ],
            [
                (Letter.F, 4),
                (Letter.F, 5),
                (Letter.F, 6),
            ],
            [
                (Letter.A, 1),
                (Letter.B, 1),
                (Letter.C, 1),
            ],
            [
                (Letter.A, 1),
                (Letter.B, 2),
            ],
        ],
    ]

    enemyFleet = GameController.initialize_ships()
    layout = start_positions[random.randint(0, len(start_positions)-1)]
    for ship_number, positions in enumerate(layout):
        for position in positions:
            enemyFleet[ship_number].positions.append(Position(*position))

    # enemyFleet[0].positions.append(Position(Letter.B, 4))
    # enemyFleet[0].positions.append(Position(Letter.B, 5))
    # enemyFleet[0].positions.append(Position(Letter.B, 6))
    # enemyFleet[0].positions.append(Position(Letter.B, 7))
    # enemyFleet[0].positions.append(Position(Letter.B, 8))
    #
    # enemyFleet[1].positions.append(Position(Letter.E, 6))
    # enemyFleet[1].positions.append(Position(Letter.E, 7))
    # enemyFleet[1].positions.append(Position(Letter.E, 8))
    # enemyFleet[1].positions.append(Position(Letter.E, 9))
    #
    # enemyFleet[2].positions.append(Position(Letter.A, 3))
    # enemyFleet[2].positions.append(Position(Letter.B, 3))
    # enemyFleet[2].positions.append(Position(Letter.C, 3))
    #
    # enemyFleet[3].positions.append(Position(Letter.F, 8))
    # enemyFleet[3].positions.append(Position(Letter.G, 8))
    # enemyFleet[3].positions.append(Position(Letter.H, 8))
    #
    # enemyFleet[4].positions.append(Position(Letter.C, 5))
    # enemyFleet[4].positions.append(Position(Letter.C, 6))

if __name__ == '__main__':
    main()
