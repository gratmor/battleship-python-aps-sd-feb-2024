import subprocess
import unittest

class TestBattleship(unittest.TestCase):

    @staticmethod
    def write_positions(positions, process):
        for position in positions:
            process.stdin.write(position.encode())
            process.stdin.write(b"\n")


if '__main__' == __name__:
    unittest.main()
